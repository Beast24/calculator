var screen= document.getElementById('screen');
var buttons= document.querySelectorAll('button');
var screenValue= '';

for (item of buttons){
    item.addEventListener('click',function(e){
        buttonText= e.target.innerText;

        if (buttonText == 'C'){
            screenValue= "";
            screen.value = screenValue;
        }

        else if(buttonText == '='){
            screen.value = eval(screenValue);
        }

        else{
            screenValue += buttonText;
            screen.value = screenValue;
        }
    })   
}

screen.addEventListener('input', function(e){
    screenValue= e.target.value;
})

screen.addEventListener('keydown', function(e){
    if(e.keyCode === 13){
        screen.value = eval(screenValue);
    }

    else if(!((e.keyCode>=48 && e.keyCode<=57) || e.keyCode===13 || e.keyCode===16 ||  e.keyCode===187 || e.keyCode===189 || e.keyCode===56 || e.keyCode===191 || e.keyCode===8 || e.keyCode===190)){
        alert("Enter an integer")
    }
})


